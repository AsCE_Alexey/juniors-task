import React from 'react'
import { connect } from 'react-redux'
import { getFiltered } from '../reducer/posts.js'

export function Posts({ posts }) {
  if (posts == 0) {
    return (
      <div className="alert alert-info">
      Нету постов
      </div>
    )
  }
  return (
    <div>
      {
        posts.map(post =>
          <div key={post.id}>
            <h3 className="alert alert-info">
              {post.id} {" "} {post.name}
            </h3>
            <h5 className="navbar"
              style={{
                backgroundColor: '#e3e3e3',
                padding: '40px'
              }}
            >
              {post.text}
            </h5>
          </div>
        )
      }
    </div>
  )
}

Posts.propTypes = {
  posts: React.PropTypes.array
}

function mapStateToProps(state) {
  return {
    posts: getFiltered(state.posts, state.filter)
  }
}

export default connect(mapStateToProps)(Posts)
